FROM node

WORKDIR /
COPY package*.json ./

RUN npm install
RUN npm install aws-sdk

COPY . .

EXPOSE 8080
CMD [ "node", "./server.js" ]