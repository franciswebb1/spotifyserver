# Node server connecting to DynamoDB and exposing on Localhost #

* To run with Dockerfile:

docker run -e AWS_ACCESS_KEY_ID=<YOUR-AWS-ACCESS-KEY> -e AWS_SECRET_ACCESS_KEY=<YOUR-SECRET-ACCESS-KEY> -t -i -p "8080":"8080" node-web-server

