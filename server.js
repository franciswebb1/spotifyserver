const tracks = require('./get-tracks.js');
const parse_data = require('./parse-data.js');

async function main() {
    const tracks_data = await tracks.scanTable('track-count');

    const data = parse_data.parseJsonObject(tracks_data);

    var express = require('express');
    var app = express();

    app.get('/tracks', function(req, res){
    res.json(data);
    })

    app.listen(8080);
}

main();