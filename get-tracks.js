const scanTable = async (tableName) => {
    var AWS = require("aws-sdk");
    AWS.config.update({
    region: "us-east-2"
    });
    var documentClient = new AWS.DynamoDB.DocumentClient();

    const params = {
        TableName: tableName,
    };

    let scanResults = [];
    let items;
    do{
        items =  await documentClient.scan(params).promise();
        items.Items.forEach((item) => scanResults.push(item));
        params.ExclusiveStartKey  = items.LastEvaluatedKey;
    }while(typeof items.LastEvaluatedKey != "undefined");

    return scanResults;

};

exports.scanTable = scanTable;


