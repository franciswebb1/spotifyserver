const parseJsonObject = function (jsonObject) {
    var tracks = [];
    var counts = [];
    for (var i = 0; i < jsonObject.length; i++) {
        tracks.push(jsonObject[i].track + ' - ' + jsonObject[i].artist);
        counts.push(jsonObject[i].count);
    }
    return [tracks, counts];
  };
  
exports.parseJsonObject = parseJsonObject;
